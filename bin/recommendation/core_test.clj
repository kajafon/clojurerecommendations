(ns recommendation.core-test
  (:require [clojure.test :refer :all]
            [recommendation.core :refer :all] ))


(deftest is-null-test 
  (testing "Testing is-null funciton, when nil is passed as first parametar, function has to return second parametar of function."
           (is (=(is-null nil 9) 9))
           ))


(deftest is-null-test1 
  (testing "Testing is-null funciton, when not nil value is passed as first parametar, function has to return that value."
           (is (=(is-null 10 9) 10))
           ))

(deftest averageForSIRN-test
  (testing "Testing calculation of average for SIRN"
           (is (= (calculateAverageForSIRN {:47 10 :43 9 :21 9 :46 7 :11 10}) 7.08))
           ))

(deftest averageForISIT-test
  (testing "Testing calculation of average for ISIT"
           (is (= (calculateAverageForISIT {:45 9 :37 8 :38 10 :14 9 :42 7}) 8.325))
           ))

(deftest averageForOIRS-test
  (testing "Testing calculation of average for ORIS"
           (is (= (calculateAverageForOIRS {:23 7 :26 10 :33 9 :36 10 :41 8}) 8.8))
           ))

(deftest averageForEPOS-test
  (testing "Testing calculation of average for EPOS"
           (is (= (calculateAverageForEPOS {:34 7 :35 10 :39 10 :42 7 :44 8}) 8.35))
           ))


(deftest calculationForIT-test
  (testing "Testing calculation of recommendations for IT courses"
           (is (= (getRecomendationsForITcourses {:34 7 :35 10 :39 10  :44 8 :23 7 :26 10 :33 9 :36 10 :41 8 :45 9 :37 8 :38 10 :14 9 :42 7 :47 10 :43 9 :21 9 :46 7 :11 10})
                  {"4" 8.8 "3" 8.35 "2" 8.325 "1" 7.08}))
           ))



