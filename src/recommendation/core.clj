(ns recommendation.core
  (:require [clojure.java [jdbc :as sql]])
  (:use [recommendation.DBCommunication :as DBCom])
   (:gen-class
    :name recommendation.core
    :methods [#^{:static true} [getRecommendations [int int] java.util.HashMap]])
  )


(defn is-null  
  "Returns first parametar if is not null, else returns second parametar" 
  [a b]
  (if (nil? a) b a))



(defn calculateAverageForSIRN 
  "Calculate average for SIRN courses, with ponders." 
  [listOfMarks]   
  (+  
    (* (is-null(listOfMarks :47) 0 ) 0.2) 
    (* (is-null(listOfMarks :43) 0 ) 0.2) 
    (* (is-null(listOfMarks :21) 0 ) 0.2) 
    (* (is-null(listOfMarks :46) 0 ) 0.14) 
    (* (is-null(listOfMarks :32) 0 ) 0.08) 
    (* (is-null(listOfMarks :11) 0 ) 0.05) 
    (* (is-null(listOfMarks :18) 0 ) 0.05)
    (* (is-null(listOfMarks :27) 0 ) 0.08)                                        
    ))


(defn calculateAverageForISIT 
  "Calculate average for ISIT courses, with ponders." 
  [listOfMarks]   
  (+  
    (* (is-null(listOfMarks :45) 0 ) 0.35) 
    (* (is-null(listOfMarks :37) 0 ) 0.35) 
    (* (is-null(listOfMarks :38) 0 ) 0.125) 
    (* (is-null(listOfMarks :14) 0 ) 0.125) 
    (* (is-null(listOfMarks :32) 0 ) 0.05)
    ))

(defn calculateAverageForOIRS 
  "Calculate average for OIRS courses, with ponders."  
  [listOfMarks]  
  (+  
    (* (is-null(listOfMarks :41) 0 ) 0.2) 
    (* (is-null(listOfMarks :26) 0 ) 0.2) 
    (* (is-null(listOfMarks :23) 0 ) 0.2) 
    (* (is-null(listOfMarks :33) 0 ) 0.2) 
    (* (is-null(listOfMarks :36) 0 ) 0.2) 
    ))

(defn calculateAverageForEPOS  
  "Calculate average for EPOS courses, with ponders." 
  [listOfMarks]   
  (+  
    (* (is-null(listOfMarks :35) 0 ) 0.25) 
    (* (is-null(listOfMarks :42) 0 ) 0.25) 
    (* (is-null(listOfMarks :44) 0 ) 0.15) 
    (* (is-null(listOfMarks :39) 0 ) 0.15) 
    (* (is-null(listOfMarks :34) 0 ) 0.2)                                         
    ))


(defn getRecomendationsForITcourses 
  "Returns sorted list of recommednations for IT courses" 
  [listOfMarks]
  (let [listOfRecommendations {"1" (calculateAverageForSIRN listOfMarks),
                               "2" (calculateAverageForISIT listOfMarks),
                               "3" (calculateAverageForEPOS listOfMarks),
                               "4" (calculateAverageForOIRS listOfMarks)
                               }]
    (into (sorted-map-by (fn [key1 key2] (compare (listOfRecommendations key2 ) ( listOfRecommendations key1)))) listOfRecommendations)
    ))


(defn getRecommendations 
  "Returns  list of recommendations for passed studend and course" 
  [student course]
  (let [listOfMarks (DBCom/getListOfMarks student)]
  ( case course 1  (java.util.HashMap.(getRecomendationsForITcourses listOfMarks))
                2 (getRecomendationsForITcourses listOfMarks)
  )))


(defn -getRecommendations
  "A Java-callable wrapper around the 'getRecommendations' function."
   [student course]
   (getRecommendations student course)
  )







