(ns recommendation.DBCommunication
   (:require [clojure.java [jdbc :as sql]])
  )

(def db {:classname "com.mysql.jdbc.Driver" 
                :subprotocol "mysql" 
                :subname "//localhost:3306/konsultacije" 
                :user "root"})


(defn getListOfMarksFromDatabase [student]
(sql/query db
         ["SELECT ocena,predmet FROM studentpredmetocena WHERE student=?" student] ))

(defn rearrangeList [mapFromDatabase] (vals mapFromDatabase) )

(defn getListOfMarks [student]
  (let [listOfMarksFromDatabase (getListOfMarksFromDatabase student)]
  (let [myHashMap (into {} (map #(hash-map (keyword (str (first %))) (first (rest %))) (map rearrangeList listOfMarksFromDatabase))) ]
    myHashMap  
    )))

(getListOfMarks 1)
  
